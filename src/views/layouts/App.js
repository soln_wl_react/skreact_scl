import React from 'react'
import {Header, Footer} from '../../widgets/HeaderFooter'
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import {Gallery, Home, MatchSchedules, PlayerProfile, PointsTable, Teams, TeamSquad} from '../../pages/client'
class App extends React.Component
{
  render()
  {
    return (
        <Router>
          <div>
            <Header />
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/gallery" component={Gallery}/>
              <Route exact path="/matchschedules" component={MatchSchedules}/>
              <Route exact path="/playerprofile" component={PlayerProfile}/>
              <Route exact path="/pointstable" component={PointsTable}/>
              <Route exact path="/teams" component={Teams}/>
              <Route exact path="/teamsquad" component={TeamSquad}/>
            </Switch>
            <Footer />
          </div>
        </Router>
    );
  }
}

export default App;
