import React, {} from 'react';
import { Link } from "react-router-dom";
import { HeaderList } from './element'

export default class HeaderListBar extends React.Component{
    render(){
        return (
            <HeaderList className='skHeaderList'>
                {this.props.list.map((obj, i) => 
                    <Link key={obj.url} to={obj.url}>{obj.title}</Link>
                )}
            </HeaderList>
        )
    }
}