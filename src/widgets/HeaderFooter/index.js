import React, {} from 'react';
import {LogoIcon} from '../../atoms/icons'
import { Link } from "react-router-dom";
//import Labeltext from '../../atoms/text/text'
import {LogoWrap, HeaderWrap, FooterWrap} from './element'
import {HeaderWidgetFixture} from './fixture'
import HeaderListBar from './headerListBar'

class Header extends React.Component
{
    state = {...HeaderWidgetFixture}
    render() {
        const data = {
            ...this.state
        }
        const { HeaderLinks } = data
        return (
            <HeaderWrap className="skHeaderCont">
                <LogoWrap className="skHeaderLogoCont"><Link to={'/'}><LogoIcon/></Link></LogoWrap>                
                <HeaderListBar list={HeaderLinks} />               
            </HeaderWrap>
        )
    }    
}

class Footer extends React.Component
{
    render() {
        return (
            <FooterWrap className="skFooterCont">                
                <LogoWrap><LogoIcon/></LogoWrap>                              
            </FooterWrap>
        )
    }    
}


export { Header, Footer }
