const HeaderWidgetFixture = {
    HeaderLinks:[
        {'title':'Teams', 'url':'/teams'},
        {'title':'Schedule & Results', 'url':'/matchschedules'},
        {'title':'Points Table', 'url':'/pointstable'},
        {'title':'Gallery', 'url':'/gallery'}
    ]
}

export {HeaderWidgetFixture}