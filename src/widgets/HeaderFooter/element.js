import styled from 'styled-components'

const LogoWrap = styled.div `
    display: flex;
    justify-content: center;
    align-items: center;
    width: 6.25rem;
    .cls1{
        fill: #c6c730;
    }
    .cls2{
        fill: #ffffff;
    }
`

const HeaderWrap = styled.div `
    display: flex;
    height: 3rem;
    background-color: #008383;
    color: #FFF;
    width: 100%;
    top: 0;
    left: 0;
    text-transform: capitalize;
`

const FooterWrap = styled.div `
    height: 60px;
    background-color: #008383;
    position: fixed;
    z-index: 1;
    width: 100%;
    bottom: 0;
    left: 0;
    display: flex;
    justify-content: center;
`
const HeaderList = styled.div `
    width: 100%;
    display: flex;
    height: 100%;
    margin: 0;
    a{
        //width: 25%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 0 20px;
        color: #fff;
        font-size: 1.25rem;
    }
    
    a:hover {
        background: #028062;
    }
`
export {LogoWrap, HeaderWrap, FooterWrap, HeaderList}