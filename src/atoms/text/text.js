import React from 'react'

export default class Labeltext extends React.Component
{
    render()
    {
      return(
        <span className={this.props.className}>{this.props.text}</span>
      )
    }
}