import {Gallery} from './Gallery'
import {Home} from './Home'
import {MatchSchedules} from './MatchSchedules'
import {PlayerProfile} from './PlayerProfile'
import {PointsTable} from './PointsTable'
import {Teams} from './Teams'
import {TeamSquad} from './TeamSquad'

export {Gallery, Home, MatchSchedules, PlayerProfile, PointsTable, Teams, TeamSquad}